/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import SQL.Estoque;
import sys.fundamentation.Item;

/**
 *
 * @author Plinio
 */
public class Tests {

    public static void main(String[] args) {

        Estoque db = new Estoque();

        try {

//            db.AddItem("Denise", "Caixinhos", "Grandeses", 15);
//            Item a = db.getItem("Denise");
//            db.changeQuantidade(a, (-180));

            Item b = new Item(0, "lala", "lele", "ll", 0, 5);
            Item c = new Item(0, "lale", "lele", "ll", 0, 5);
            Item d = new Item(0, "lali", "lele", "ll", 0, 5);
            Item e = new Item(0, "lalo", "lele", "ll", 0, 5);

            Item[] j = new Item[4];
            
            j[0] = b;
            j[1] = c;
            j[2] = d;
            j[3] = e;

            db.addItens(j);

            Item[] items = db.getItens();
            for (Item a : items) {
                System.out.println("Item -------");
                System.out.println("ID: " + a.getID());
                System.out.println("Nome: " + a.getNome());
                System.out.println("Quantidade: " + a.getQuantidade());
                System.out.println("Quantidade Maior:" + a.getQuantidade_Maior() + a.getUnidade_Maior());
                System.out.println("Quantidade Menor:" + a.getQuantidade_Menor() + a.getUnidade_Menor());
            }

            db.desconect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
