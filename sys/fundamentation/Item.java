package sys.fundamentation;

public class Item {

    private int ID;
    private String Nome;
    private String Unidade_Menor;
    private String Unidade_Maior;
    private int conversao;
    private int Quantidade_Menor;

    public int getConversao() {
        return conversao;
    }

    public void setConversao(int conversao) {
        this.conversao = conversao;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUnidade_Maior() {
        return Unidade_Maior;
    }

    public void setUnidade_Maior(String Unidade_Maior) {
        this.Unidade_Maior = Unidade_Maior;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getUnidade_Menor() {
        return Unidade_Menor;
    }

    public void setUnidade_Menor(String Unidade) {
        this.Unidade_Menor = Unidade;
    }

    public int getQuantidade_Maior() {
        if (this.conversao != 0) {
            return Math.abs(this.Quantidade_Menor / this.conversao);
        } else {
            return 0;
        }
    }

    public int getQuantidade_Menor() {
        if (this.conversao != 0) {
            return this.Quantidade_Menor % this.conversao;
        } else {
            return 0;
        }
    }

    /*
     * @param valor - na menor unidade;
     */
    public void setQuantidade(int Valor) {
        this.Quantidade_Menor = Valor;
    }

    public int getQuantidade() {
        return this.Quantidade_Menor;
    }

    public void setQuantidade_Menor(int Quantidade) {
        this.Quantidade_Menor = Quantidade;
    }

    public Item(int ID, String Nome, String Unidade_Menor, String Unidade_Maior, int Quantidade_Menor, int conversao) {
        this.ID = ID;
        this.Nome = Nome;
        this.Unidade_Menor = Unidade_Menor;
        this.Unidade_Maior = Unidade_Maior;
        this.conversao = conversao;
        this.Quantidade_Menor = Quantidade_Menor;
    }

    @Override
    public String toString() {
//        return String.valueOf(this.ID);
        return this.getNome();
    }
}