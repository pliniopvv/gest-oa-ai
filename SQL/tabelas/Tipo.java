/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL.tabelas;

public class Tipo {

    private int ID;
    private String desc;

    public Tipo(int ID, String desc) {
        this.ID = ID;
        this.desc = desc;
    }

    /**
     *
     * @return Descrição;
     */
    @Override
    public String toString() {
        return this.desc;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
