/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL.cfg;

/**
 *
 * @author Plinio
 */
public abstract class CfgDb {

    //DATABASE URL
    private static final String Data = "D:\\ARQUIVOS\\@PERSONALIZACOES\\APPS\\JAVA\\GestaoAcai\\Data\\";
    private static final String Database = "DATABASE.FDB";
    //DATABASE USER
    private static final String USER = "SYSDBA";
    private static final String PASS = "masterkey";

    public static String getDatabaseUrl() {
        return CfgDb.Data + CfgDb.Database;
    }

    public static String getUSER() {
        return USER;
    }

    public static String getPASS() {
        return PASS;
    }
}
