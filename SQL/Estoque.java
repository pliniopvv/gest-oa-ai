/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package SQL;

import Firebird.ConexaoFirebird;
import sys.fundamentation.Item;
import SQL.cfg.CfgDb;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Plinio
 */
public class Estoque {

    private final ConexaoFirebird db = new ConexaoFirebird(CfgDb.getDatabaseUrl());

    private String getData() {
        Date a = new Date();
        SimpleDateFormat b = new SimpleDateFormat("yyyy-MM-dd");
        return b.format(a);
    }

    public Estoque() {
        db.conectar(CfgDb.getUSER(), CfgDb.getPASS());
    }

    public void createDatabases() throws SQLException {
        new Throwable("Não Implementado.");
    }

    public void addItem(Item item) throws SQLException {
        String SQL = "INSERT INTO ESTOQUE_ITEM"
                + "(ID, NOME, UNIDADE_MAIOR, UNIDADE_MENOR, CONVERSAO) VALUES"
                + "(0, '" + item.getNome() + "', '" + item.getUnidade_Maior() + "', '" + item.getUnidade_Menor() + "', " + item.getConversao() + ");";
        db.executeUpdate(SQL);

        SQL = "SELECT ID FROM ESTOQUE_ITEM WHERE NOME LIKE '" + item.getNome() + "'";
        db.executaQuery(SQL);
        db.resultset.first();
        int ID = db.resultset.getInt("ID");

        SQL = "INSERT INTO ESTOQUE_QUANTIDADE (ID_ITEM, QUANTIDADE_MENOR) VALUES (" + ID + ", 0);";
        db.executeUpdate(SQL);
    }

    public void addItens(Item[] itens) throws SQLException {
        String SQL = "EXECUTE BLOCK AS BEGIN ";
        for (Item item : itens) {
            SQL += " INSERT INTO ESTOQUE_ITEM"
                    + "(ID, NOME, UNIDADE_MAIOR, UNIDADE_MENOR, CONVERSAO) VALUES"
                    + "(0, '" + item.getNome() + "', '" + item.getUnidade_Maior() + "', '" + item.getUnidade_Menor() + "', " + item.getConversao() + ");";
        }
        SQL += " END";
        db.executeUpdate(SQL);

        SQL = "SELECT ESTOQUE_ITEM.ID FROM ESTOQUE_ITEM WHERE ";
        for (Item item : itens) {
            SQL += " (ESTOQUE_ITEM.NOME = '" + item.getNome() + "')";
            if (!itens[(itens.length - 1)].getNome().equals(item.getNome())) {
                SQL += " Or";
            }
        }

        db.executaQuery(SQL);
        db.resultset.first();
        ArrayList<Integer> a = new ArrayList<Integer>();
        do {
            a.add(db.resultset.getInt("ID"));
        } while (db.resultset.next());

        SQL = " EXECUTE BLOCK AS BEGIN";
        for (Integer b : a) {
            SQL += " INSERT INTO ESTOQUE_QUANTIDADE (ID_ITEM, QUANTIDADE_MENOR) VALUES (" + b + ",0);";
        }
        SQL += " END";

        db.executeUpdate(SQL);
    }

    public void changeQuantidade(Item item, int Quantidade) throws SQLException {
        int nQ = (item.getQuantidade() + Quantidade);
        System.out.println(Quantidade);
        System.out.println(item.getQuantidade());
        String SQL = "UPDATE ESTOQUE_QUANTIDADE SET QUANTIDADE_MENOR = " + nQ + " WHERE ID_ITEM = " + item.getID() + ";";
        System.out.println("--:SQL: " + SQL);
        db.executeUpdate(SQL);
        item.setQuantidade(nQ);
    }

    public Item[] getItens() throws SQLException {
        String SQL = "Select"
                + "  ESTOQUE_ITEM.ID,"
                + "  ESTOQUE_ITEM.NOME,"
                + "  ESTOQUE_ITEM.UNIDADE_MAIOR,"
                + "  ESTOQUE_ITEM.UNIDADE_MENOR,"
                + "  ESTOQUE_ITEM.CONVERSAO,"
                + "  ESTOQUE_QUANTIDADE.QUANTIDADE_MENOR"
                + "  From"
                + "  ESTOQUE_QUANTIDADE Inner Join"
                + "  ESTOQUE_ITEM On ESTOQUE_ITEM.ID = ESTOQUE_QUANTIDADE.ID_ITEM";
        ArrayList<Item> a = new ArrayList<Item>();
        db.executaQuery(SQL);
        db.resultset.first();
        do {
            int ID = db.resultset.getInt("ID");
            String Nome = db.resultset.getString("NOME");
            int Convesao = db.resultset.getInt("CONVERSAO");
            String Unidade_Maior = db.resultset.getString("UNIDADE_MAIOR");
            String Unidade_Menor = db.resultset.getString("UNIDADE_MENOR");
            int Quantidade_Menor = db.resultset.getInt("QUANTIDADE_MENOR");
            a.add(new Item(ID, Nome, Unidade_Menor, Unidade_Maior, Quantidade_Menor, Convesao));
        } while (db.resultset.next());
        return a.toArray(new Item[a.size()]);
    }

    public Item getItem(String Nome) throws SQLException {
        String SQL = "Select"
                + "  ESTOQUE_ITEM.UNIDADE_MAIOR,"
                + "  ESTOQUE_ITEM.UNIDADE_MENOR,"
                + "  ESTOQUE_QUANTIDADE.QUANTIDADE_MENOR,"
                + "  ESTOQUE_ITEM.CONVERSAO,"
                + "  ESTOQUE_ITEM.ID"
                + "  From"
                + "  ESTOQUE_QUANTIDADE Inner Join"
                + "  ESTOQUE_ITEM On ESTOQUE_ITEM.ID = ESTOQUE_QUANTIDADE.ID_ITEM"
                + "  Where"
                + "  ESTOQUE_ITEM.NOME LIKE '" + Nome + "';";
        db.executaQuery(SQL);
        db.resultset.first();

        int ID = db.resultset.getInt("ID");
        int C = db.resultset.getInt("CONVERSAO");
        int qM = db.resultset.getInt("QUANTIDADE_MENOR");
        String n = Nome;
        String uM = db.resultset.getString("UNIDADE_MAIOR");
        String um = db.resultset.getString("UNIDADE_MENOR");

        Item retorno = new Item(ID, n, um, uM, qM, C);
        return retorno;
    }

    public Item getItem(int ID) {
        String SQL = "Select"
                + "  ESTOQUE_ITEM.NOME,"
                + "  ESTOQUE_ITEM.UNIDADE_MAIOR,"
                + "  ESTOQUE_ITEM.UNIDADE_MENOR,"
                + "  ESTOQUE_QUANTIDADE.QUANTIDADE_MENOR,"
                + "  ESTOQUE_ITEM.CONVERSAO"
                + "  From"
                + "  ESTOQUE_QUANTIDADE Inner Join"
                + "  ESTOQUE_ITEM On ESTOQUE_ITEM.ID = ESTOQUE_QUANTIDADE.ID_ITEM"
                + "  Where"
                + "  ESTOQUE_ITEM.ID = " + ID + "";
        Item retorno = null;
        try {
            db.executaQuery(SQL);
            db.resultset.first();

            int C = db.resultset.getInt("CONVERSAO");
            int qM = db.resultset.getInt("QUANTIDADE_MENOR");
            String n = db.resultset.getString("NOME");
            String uM = db.resultset.getString("UNIDADE_MAIOR");
            String um = db.resultset.getString("UNIDADE_MENOR");

            retorno = new Item(ID, n, um, uM, C, qM);
        } catch (Exception e) {
        }

        return retorno;
    }

    public void desconect() {
        db.desconectar();
    }
}
